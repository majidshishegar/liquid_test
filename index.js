const path = require('path');
const { Liquid } = require('liquidjs');
const engine = new Liquid({
    root: path.resolve(__dirname, 'views/'),
    extname: '.btml'
});

const { where_function, get_function, test_function } = require('./filters');
engine.registerFilter('where', where_function);
engine.registerFilter('get', get_function);
engine.registerFilter('test', test_function);


const app = require('express')();

app.get('/', (req, res) => {
    return res.send(engine.renderFileSync('home', { ProductCollection: 'products' }));
});

const listen = async (port = 3000) => {
    await app.listen(port);
    console.log(`running on http://localhost:${port}`);
}
listen();