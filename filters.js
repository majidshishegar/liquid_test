
const { get_products } = require('./api');

const where_function = (query, ...options) => {
    if (typeof query !== 'object') {
        query = { collection_name: query };
        console.log('query initialized with: ', query);
    }
    let new_query = {};
    if (options.length == 2)
        new_query = { [options[0]]: { operator: 'eq', value: options[1] } };
    else if (options.length == 3)
        new_query = { [options[0]]: { operator: options[1], value: options[2] } };

    console.log('adding to query', new_query);
    query = { ...query, ...new_query };
    return query;
};

const get_function = (query) => {
    console.log('getting query!', query);

    if (typeof query !== 'object')
        query = { collection_name: query };

    return get_products(query);
};

const array_to_object = (array) => {
    const obj = {};
    for (i in array) {
        obj[array[i][0]] = array[i][1]
    }
    return obj;
}

const test_function = (query, ...args) => {
    const new_args = array_to_object(args);
    console.log('test args is :', { query, new_args });
    return query;
}

module.exports = {
    where_function,
    get_function,
    test_function,
}