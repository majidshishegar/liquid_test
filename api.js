const all_products = [
    { id: 1, title: "product 1", type: "x" },
    { id: 2, title: "product 2", type: "y" },
    { id: 3, title: "product 3", type: "y" },
    { id: 4, title: "product 4", type: "z" },
    { id: 5, title: "product 5", type: "z" },
    { id: 6, title: "product 6", type: "z" },
    { id: 7, title: "product 7", type: "z" },
    { id: 8, title: "product 8", type: "z" },
    { id: 9, title: "product 9", type: "z" },
    { id: 10, title: "product 10", type: "w" },
    { id: 11, title: "product 11", type: "w" },
    { id: 12, title: "product 12", type: "w" },
    { id: 13, title: "product 13", type: "w" },
];

const compare = (value1, value2, operator) => {
    switch (operator) {
        case 'lt':
            return value1 < value2;
        case 'lte':
            return value1 <= value2;
        case 'mt':
            return value1 > value2;
        case 'mte':
            return value1 >= value2;
        case 'eq':
            return value1 === value2;
        case 'ne':
            return value1 !== value2;
        default:
            return value1 === value2;

    }
}

const get_products = (query) => {
    const filtered_products = all_products.filter((product) => {
        let result = true;

        const keys = Object.keys(query);
        for (i in keys) {
            result &= compare(product[keys[i]], query[keys[i]].value, query[keys[i]].operator || 'eq');
        }

        return result;
    });
    return filtered_products;
}

module.exports = {
    get_products
}